angular.module('test-app')

    .directive('news', ['User', '$rootScope', function(User, $rootScope){

    var title = 'asd';
    
    console.log('cosas que pasan antes de construir la directiva.');

    User.getLastName().then(function(data){
        console.log('por fin llego: ' + data);
    });

    return {
        restrict: 'E',
        templateUrl: 'views/directives/news-directive.html',
        link: function(scope, element, attrs){
            User.getName('tt1285016').then(function(response){
                scope.title = response.data.Title;
            }, function(error){
                console.log(error);
            });

        }
    };
}]);
