angular.module('test-app')
.service('User', ['$http', '$q', function($http, $q){

    this.getName = function(id){
        return $http.get('http://www.omdbapi.com/?i=' + id);
    };

    this.getLastName = function(){
        var defer = $q.defer();

        setTimeout(function(){
            defer.resolve('hola mundillo');
        }, 2000);

        return defer.promise;
    };

    this.getAge = function(){
        return 18;
    };

}]);
